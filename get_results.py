import os
import glob
import pandas as pd


def summary_results(path_list, show_details=False):
    df_dic = {} 
    for path in path_list:
        if not os.path.exists(path):
            print(path, "not exist")
            continue
        df = pd.read_csv(path, index_col=0)   
        name = os.path.basename(path)
        if show_details:
            print(name)
            print(df)
        df_dic.update({name: df.iloc[-1]})
    print(pd.DataFrame(df_dic).T)


def check_train_info(path):
    print("check info", path)
    data = open(path).read()
    print(data)



def main():
    MAIN_DIR = "/home/johncl/repos/ood/cider/results/"
    INFO_DIR = "/home/johncl/repos/ood/cider/logs/"

    dataset = "CIFAR-100"
    checkpoints = [
        "01_06_01:47_cider_resnet34_lr_0.05_cosine_True_bsz_128_cider_wd_10.0_500_64_trial_0_temp_0.1_CIFAR-100_pm_0.5",
        "04_05_08:50_cider_resnet34_lr_0.5_cosine_True_bsz_512_cider_wd_1.0_500_128_trial_0_temp_0.1_CIFAR-100_pm_0.5",
        "12_05_04:50_sf_resnet34_lr_0.5_cosine_True_bsz_256_sf_500_128_trial_0_temp_0.1_CIFAR-100_mlp"
        
    ]


    model = "cider"
    epoch = 500
    score = "knn"

    print(f"Use score function: {score}")
    path = f"{MAIN_DIR}/{dataset}/{{0}}/{model}/epoch_{epoch}/{score}/{{0}}.csv"
    path_list = [path.format(checkpoint) for checkpoint in checkpoints]
    summary_results(path_list, show_details=True)


    path = f"{MAIN_DIR}/{dataset}/{{0}}/train_args.txt"
    path_list = [path.format(checkpoint) for checkpoint in checkpoints]
    #check_train_info(path_list[1].replace(MAIN_DIR, INFO_DIR))

if __name__ == "__main__":
    main()
