# Learning Multi-Manifold Embedding for Out-Of-Distribution Detection
This paper is the official implementation of Multi-Manifold Embedding Learning (MMEL).
Our [paper](https://arxiv.org/abs/2409.12479) has been selected for the **Best Paper Award** at the [ECCV 2024 Workshop on Beyond Euclidean](https://sites.google.com/view/beyondeuclidean/home).
We acknowledge the codebase released from [CIDER](https://github.com/deeplearning-wisc/cider).

### Abstract
Out-of-distribution (OOD) detection is essential to build trustworthy artificial intelligence in real-world applications. Numerous scoring algorithms are developed to estimate the distribution that lies far beyond the scope of the training data. Recent advancements in representation learning open a novel gateway to detect OOD based on the extracted latent embeddings. However, a single embedding space is insufficient to characterize in-distribution data and defend a diverse array of OOD conditions. This work proposes a **Multi-Manifold Embedding Learning (MMEL)** framework to enhance the OOD detection capability. We jointly optimize the hypersphere and hyperbolic spaces to embed representative information to differentiate OOD samples. Evaluations are performed on six open datasets for detecting OOD samples in the metric of AUC and FPR, for comparison against the recent distance-based OOD detection methods. Our proposed MMEL framework significantly reduces the FPR while keeping a high AUC. We further analyze the effects of learning multiple manifolds and visualize the OOD score distributions among various OOD datasets. This further reveals the advantage of embedding learning: the FPR can be significantly reduced by enrolling very few OOD samples without model retraining. The implementation code will be released upon paper acceptance.

### Illustration

![fig1](readme_figs/framework.png)



## Quick Start

### Data Preparation

The default root directory for ID and OOD datasets is `datasets/`. We consider the following (in-distribution) datasets: CIFAR-10, CIFAR-100, and ImageNet-100. 

**Small-scale OOD datasets** For small-scale ID (e.g. CIFAR-10), we use SVHN, Textures (dtd), Places365, LSUN-C (LSUN), LSUN-R (LSUN_resize), and iSUN. 

OOD datasets can be downloaded via the following links (source: [ATOM](https://github.com/jfc43/informative-outlier-mining/blob/master/README.md)):

* [SVHN](http://ufldl.stanford.edu/housenumbers/test_32x32.mat): download it and place it in the folder of `datasets/small_OOD_dataset/svhn`. Then run `python utils/select_svhn_data.py` to generate test subset.
* [Textures](https://www.robots.ox.ac.uk/~vgg/data/dtd/download/dtd-r1.0.1.tar.gz): download it and place it in the folder of `datasets/small_OOD_dataset/dtd`.
* [Places365](http://data.csail.mit.edu/places/places365/test_256.tar): download it and place it in the folder of `datasets/ood_datasets/places365/test_subset`. We randomly sample 10,000 images from the original test dataset. 
* [LSUN-C](https://www.dropbox.com/s/fhtsw1m3qxlwj6h/LSUN.tar.gz): download it and place it in the folder of `datasets/small_OOD_dataset/LSUN`.
* [LSUN-R](https://www.dropbox.com/s/moqh2wh8696c3yl/LSUN_resize.tar.gz): download it and place it in the folder of `datasets/small_OOD_dataset/LSUN_resize`.
* [iSUN](https://www.dropbox.com/s/ssz7qxfqae0cca5/iSUN.tar.gz): download it and place it in the folder of `datasets/small_OOD_dataset/iSUN`.
* [CIFAR100-C](https://zenodo.org/record/3555552/files/CIFAR-100-C.tar?download=1): dowload it and place it in the folder of `datasets/small_OOD_dataset/CIFAR-100-C`.


The directory structure looks like:

```python
datasets/
---CIFAR10/
---CIFAR100/
---places365/
---small_OOD_dataset/
------dtd/
------iSUN/
------LSUN/
------LSUN_resize/
------SVHN/
------CIFAR-100-C/
```


**Large-scale OOD datasets** For large-scale ID (e.g. ImageNet-100), we use the curated 4 OOD datasets from [iNaturalist](https://arxiv.org/pdf/1707.06642.pdf), [SUN](https://vision.princeton.edu/projects/2010/SUN/paper.pdf), [Places](http://places2.csail.mit.edu/PAMI_places.pdf), and [Textures](https://arxiv.org/pdf/1311.3618.pdf), and de-duplicated concepts overlapped with ImageNet-1k. The datasets are created by  [Huang et al., 2021](https://github.com/deeplearning-wisc/large_scale_ood) .

The subsampled iNaturalist, SUN, and Places can be download via the following links:

```
wget http://pages.cs.wisc.edu/~huangrui/imagenet_ood_dataset/iNaturalist.tar.gz
wget http://pages.cs.wisc.edu/~huangrui/imagenet_ood_dataset/SUN.tar.gz
wget http://pages.cs.wisc.edu/~huangrui/imagenet_ood_dataset/Places.tar.gz

```
The directory structure looks like:
```python
datasets/
---ImageNet100/
---ImageNet_OOD_dataset/
------dtd/
------iNaturalist/
------Places/
------SUN/
```


## Training and Evaluation 

### Model Checkpoints

**Evaluate pre-trained checkpoints** 

Our checkpoints can be downloaded [here](https://inventeccorp.sharepoint.com/:f:/r/sites/msteams_3f827a_968053-ResearchSquad/Shared%20Documents/Research%20Squad/submissions/AAAI2024-OOD/ood/checkpoints?csf=1&web=1&e=yxL7XE). Put the checkpoints in the `ckpt_c10` or `ckpt_c100` folder following the structure below:

```
checkpoints/
---CIFAR-10/	 	
------ckpt_c10/
------checkpoint_500.pth.tar
---CIFAR-100/	 	
------ckpt_c100/
------checkpoint_500.pth.tar
```

The following scripts can be used to evaluate the OOD detection performance:

```
sh scripts/eval_ckpt_cifar10.sh ckpt_c10 #for CIFAR-10
sh scripts/eval_ckpt_cifar100.sh ckpt_c100 # for CIFAR-100
```



**Evaluate custom checkpoints** 

If the default directory to save checkpoints is not `checkpoints`, create a softlink to the directory where the actual checkpoints are saved and name it as `checkpoints`. For example, checkpoints for CIFAR-100 (ID) are structured as follows: 

```python
checkpoints/
---CIFAR-100/
------name_of_ckpt/
---------checkpoint_5.pth.tar
---------checkpoint_10.pth.tar
```



**Train from scratch** 

We provide sample scripts to train from scratch. Feel free to modify the hyperparameters and training configurations.

```
sh scripts/train_cifar10.sh
sh scripts/train_cifar100.sh
```

**Fine-tune from ImageNet pre-trained models** 

We also provide fine-tuning scripts on large-scale datasets such as ImageNet-100.

```
sh scripts/train_imgnet100.sh  # To be updated
```




### Citation

If you find our work useful, please consider citing our paper:

```
@article{li2024learning,
  title={Learning Multi-Manifold Embedding for Out-Of-Distribution Detection},
  author={Li, Jeng-Lin and Chang, Ming-Ching and Chen, Wei-Chao},
  journal={arXiv preprint arXiv:2409.12479},
  year={2024}
}
```

