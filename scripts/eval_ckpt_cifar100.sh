NAME=$1
ID_DATASET=CIFAR-100 
ID_LOC=../cider/datasets/CIFAR100
OOD_LOC=../cider/datasets/small_OOD_dataset


python eval_ood.py \
        --epoch 500 \
        --model resnet34 \
        --head manifold \
        --gpu 0\
        --score knn\
        --K 300 \
        --feat_dim 128 \
	--embedding_dim 512 \
        --in_dataset ${ID_DATASET} \
        --id_loc ${ID_LOC} \
        --ood_loc ${OOD_LOC} \
        --name ${NAME} \
	--main_dir ../cider/ \
	--ash_method "" \
	-b 512\
        --c_ball 0.01
