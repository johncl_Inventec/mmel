NAME=$1
ID_DATASET=CIFAR-10 
ID_LOC=../cider/datasets/CIFAR10
OOD_LOC=../cider/datasets/small_OOD_dataset


python eval_ood.py \
        --epoch 500 \
        --model resnet18 \
        --head mlp \
        --gpu 1 \
        --score knn\
        --K 100 \
        --in_dataset ${ID_DATASET} \
        --id_loc ${ID_LOC} \
        --ood_loc ${OOD_LOC} \
        --name ${NAME} \
	--main_dir ../cider/ \
        --ash_method "ash_s@10" 
