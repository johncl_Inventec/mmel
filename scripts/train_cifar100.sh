python train.py \
    --in-dataset CIFAR-100\
    --id_loc ../cider/datasets/CIFAR100 \
    --gpu 0 \
    --seed 7 \
    --head manifold \
    --model resnet34 \
    --loss mmel_ce \
    --epochs 500 \
    --learning_rate 0.5\
    --proto_m 0.5 \
    --feat_dim 128 \
    --w 1 \
    --batch-size 128 \
    --cosine \
    --main_dir ../cider/ \
    --c_ball 0.01 \

