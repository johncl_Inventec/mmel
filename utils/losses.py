
"""
Aapted from SupCon: https://github.com/HobbitLong/SupContrast/
"""
from __future__ import print_function

import os
import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import time
import numpy as np

import models.hyptorch as hypb


def binarize(T, nb_classes):
    T = T.cpu().numpy()
    import sklearn.preprocessing
    T = sklearn.preprocessing.label_binarize(
        T, classes = range(0, nb_classes)
    )
    T = torch.FloatTensor(T).cuda()
    return T

def l2_norm(input):
    input_size = input.size()
    buffer = torch.pow(input, 2)
    normp = torch.sum(buffer, 1).add_(1e-12)
    norm = torch.sqrt(normp)
    _output = torch.div(input, norm.view(-1, 1).expand_as(input))
    output = _output.view(input_size)
    return output


class Proxy_Anchor(torch.nn.Module):
    def __init__(self, nb_classes, sz_embed, mrg = 0.1, alpha = 32):
        torch.nn.Module.__init__(self)
        # Proxy Anchor Initialization
        self.proxies = torch.nn.Parameter(torch.randn(nb_classes, sz_embed).cuda())
        nn.init.kaiming_normal_(self.proxies, mode='fan_out')

        self.nb_classes = nb_classes
        self.sz_embed = sz_embed
        self.mrg = mrg
        self.alpha = alpha
        
    def forward(self, X, T):
        P = self.proxies

        cos = F.linear(l2_norm(X), l2_norm(P))  # Calcluate cosine similarity
        P_one_hot = binarize(T = T, nb_classes = self.nb_classes)
        N_one_hot = 1 - P_one_hot
    
        pos_exp = torch.exp(-self.alpha * (cos - self.mrg))
        neg_exp = torch.exp(self.alpha * (cos + self.mrg))

        with_pos_proxies = torch.nonzero(P_one_hot.sum(dim = 0) != 0).squeeze(dim = 1)   # The set of positive proxies of data in the batch
        num_valid_proxies = len(with_pos_proxies)   # The number of positive proxies
        
        P_sim_sum = torch.where(P_one_hot == 1, pos_exp, torch.zeros_like(pos_exp)).sum(dim=0) 
        N_sim_sum = torch.where(N_one_hot == 1, neg_exp, torch.zeros_like(neg_exp)).sum(dim=0)
        
        pos_term = torch.log(1 + P_sim_sum).sum() / num_valid_proxies
        neg_term = torch.log(1 + N_sim_sum).sum() / self.nb_classes
        loss = pos_term + neg_term     
        
        return loss

class SupConLoss(nn.Module):
    """Supervised Contrastive Learning: https://arxiv.org/pdf/2004.11362.pdf.
    It also supports the unsupervised contrastive loss in SimCLR"""
    def __init__(self, args, contrast_mode='all', base_temperature=0.07):
        super(SupConLoss, self).__init__()
        self.args = args
        self.temperature = args.temp
        self.contrast_mode = contrast_mode
        self.base_temperature = base_temperature

    def forward(self, features, labels=None, mask=None):
        """Compute loss for model. If both `labels` and `mask` are None,
        it degenerates to SimCLR unsupervised loss:
        https://arxiv.org/pdf/2002.05709.pdf
        Args:
            features: hidden vector of shape [bsz, n_views, ...].
            labels: ground truth of shape [bsz].
            mask: contrastive mask of shape [bsz, bsz], mask_{i,j}=1 if sample j
                has the same class as sample i. Can be asymmetric.
        Returns:
            A loss scalar.
        """
        device = (torch.device('cuda')
                  if features.is_cuda
                  else torch.device('cpu'))

        if len(features.shape) < 3:
            raise ValueError('`features` needs to be [bsz, n_views, ...],'
                             'at least 3 dimensions are required')
        if len(features.shape) > 3:
            features = features.view(features.shape[0], features.shape[1], -1)

        batch_size = features.shape[0]
        if labels is not None and mask is not None:
            raise ValueError('Cannot define both `labels` and `mask`')
        elif labels is None and mask is None:
            mask = torch.eye(batch_size, dtype=torch.float32).to(device)
        elif labels is not None:
            labels = labels.contiguous().view(-1, 1)
            if labels.shape[0] != batch_size:
                raise ValueError('Num of labels does not match num of features')
            mask = torch.eq(labels, labels.T).float().to(device)
        else:
            mask = mask.float().to(device)

        contrast_count = features.shape[1]
        contrast_feature = torch.cat(torch.unbind(features, dim=1), dim=0)
        if self.contrast_mode == 'one':
            anchor_feature = features[:, 0]
            anchor_count = 1
        elif self.contrast_mode == 'all':
            anchor_feature = contrast_feature
            anchor_count = contrast_count
        else:
            raise ValueError('Unknown mode: {}'.format(self.contrast_mode))

        # compute logits
        if self.args.head == "hypb":
            anchor_dot_contrast = -hypb.dist_matrix(anchor_feature, 
                contrast_feature, 
                c=self.args.c_ball
            )
        else:
            anchor_dot_contrast = torch.div(
                torch.matmul(anchor_feature, contrast_feature.T),
                self.temperature
            )

        # for numerical stability
        logits_max, _ = torch.max(anchor_dot_contrast, dim=1, keepdim=True)
        logits = anchor_dot_contrast - logits_max.detach()

        # tile mask
        mask = mask.repeat(anchor_count, contrast_count)
        # mask-out self-contrast cases
        logits_mask = torch.scatter(
            torch.ones_like(mask),
            1,
            torch.arange(batch_size * anchor_count).view(-1, 1).to(device),
            0
        )
        mask = mask * logits_mask

        # compute log_prob
        exp_logits = torch.exp(logits) * logits_mask
        log_prob = logits - torch.log(exp_logits.sum(1, keepdim=True))

        # compute mean of log-likelihood over positive
        mean_log_prob_pos = (mask * log_prob).sum(1) / mask.sum(1)

        # loss
        loss = - (self.temperature / self.base_temperature) * mean_log_prob_pos
        loss = loss.view(anchor_count, batch_size).mean()

        return loss

class CompLoss(nn.Module):
    '''
    Compactness Loss with class-conditional prototypes
    '''
    def __init__(self, args, temperature=0.07, base_temperature=0.07):
        super(CompLoss, self).__init__()
        self.args = args
        self.temperature = temperature
        self.base_temperature = base_temperature

    def forward(self, features, prototypes, labels):
        proxy_labels = torch.arange(0, self.args.n_cls).cuda()
        labels = labels.contiguous().view(-1, 1)
        mask = torch.eq(labels, proxy_labels.T).float().cuda() #bz, cls

        # compute logits
        if self.args.head == "hypb":
            feat_dot_prototype = -hypb.dist_matrix(features, prototypes, c=self.args.c_ball)
        else:
            prototypes = F.normalize(prototypes, dim=1) 
            feat_dot_prototype = torch.matmul(features, prototypes.T)
        feat_dot_prototype = torch.div(feat_dot_prototype, self.temperature)
        # for numerical stability
        logits_max, _ = torch.max(feat_dot_prototype, dim=1, keepdim=True)
        logits = feat_dot_prototype - logits_max.detach()

        # compute log_prob
        exp_logits = torch.exp(self.args.r*logits-self.args.margin) 
        log_prob = logits - torch.log(
            exp_logits.sum(1, keepdim=True) 
        )

        # compute mean of log-likelihood over positive
        mean_log_prob_pos = (mask * log_prob).sum(1) 

        # loss
        loss = - (self.temperature / self.base_temperature) * mean_log_prob_pos.mean()
        return loss


class DisLPLoss(nn.Module):
    '''
    Dispersion Loss with learnable prototypes
    '''
    def __init__(self, args, model, loader, temperature= 0.1, base_temperature=0.1):
        super(DisLPLoss, self).__init__()
        self.args = args
        self.temperature = temperature
        self.base_temperature = base_temperature
        self.model = model
        self.loader = loader
        self.init_class_prototypes()

    def compute(self):
        num_cls = self.args.n_cls
        # l2-normalize the prototypes if not normalized
        prototypes = F.normalize(self.prototypes, dim=1) 

        labels = torch.arange(0, num_cls).cuda()
        labels = labels.contiguous().view(-1, 1)

        mask = (1- torch.eq(labels, labels.T).float()).cuda()

        logits = torch.div(
            torch.matmul(prototypes, prototypes.T),
            self.temperature)

        mean_prob_neg = torch.log((mask * torch.exp(logits)).sum(1) / mask.sum(1))
        mean_prob_neg = mean_prob_neg[~torch.isnan(mean_prob_neg)]
        # loss
        loss = self.temperature / self.base_temperature * mean_prob_neg.mean()

        return loss
    
    def init_class_prototypes(self):
        """Initialize class prototypes"""
        self.model.eval()
        start = time.time()
        prototype_counts = [0]*self.args.n_cls
        with torch.no_grad():
            prototypes = torch.zeros(self.args.n_cls,self.args.feat_dim).cuda()
            for input, target in self.loader:
                input, target = input.cuda(), target.cuda()
                features = self.model.head_forward(input) # extract normalized features
                for j, feature in enumerate(features):
                    prototypes[target[j].item()] += feature
                    prototype_counts[target[j].item()] += 1
            for cls in range(self.args.n_cls):
                prototypes[cls] /=  prototype_counts[cls] 
            # measure elapsed time
            duration = time.time() - start
            print(f'Time to initialize prototypes: {duration:.3f}')
            prototypes = F.normalize(prototypes, dim=1)
            self.prototypes = torch.nn.Parameter(prototypes)

class DisLoss(nn.Module):
    '''
    Dispersion Loss with EMA prototypes
    '''
    def __init__(self, args, model, loader, temperature= 0.1, base_temperature=0.1):
        super(DisLoss, self).__init__()
        self.args = args
        self.temperature = temperature
        self.base_temperature = base_temperature
        self.register_buffer("prototypes", 
            torch.zeros(self.args.n_cls, self.args.feat_dim)
        )
        self.model = model
        self.loader = loader
        self.init_class_prototypes()

    def forward(self, features, labels):    
        prototypes = self.prototypes
        num_cls = self.args.n_cls
        if self.args.head == "hypb":
            for i_cls in range(self.args.n_cls): 
                feats = torch.vstack([features[j] for j in range(len(features)) 
                    if labels[j] == labels[i_cls] 
                ])
                cur_proto = hypb.poincare_mean(feats, dim=0, c=self.args.c_ball)
                prototypes[i_cls] = prototypes[i_cls]*self.args.proto_m + \
                    cur_proto*(1-self.args.proto_m)
        else:
            for j in range(len(features)):
                cls_ = labels[j].item()
                prototypes[cls_] = F.normalize(
                    prototypes[cls_]*self.args.proto_m +  
                    features[j]*(1-self.args.proto_m), 
                    dim=0
                )

        self.prototypes = prototypes.detach()
        labels = torch.arange(0, num_cls).cuda()
        labels = labels.contiguous().view(-1, 1)

        mask = (1-torch.eq(labels, labels.T).float()).cuda()


        # compute disperity between prototypes
        if self.args.head == "hypb":
            logits = -hypb.dist_matrix(prototypes, 
                prototypes, c=self.args.c_ball
            )
        else:
            logits = torch.matmul(prototypes, prototypes.T)
        logits = torch.div(logits, self.temperature) # (n_cls, n_cls) 

        # for numerical stability
        # logits_max, _ = torch.max(logits, dim=1, keepdim=True)
        # logits = logits - logits_max.detach()

        logits_mask = torch.scatter(torch.ones_like(mask), 1,
            torch.arange(num_cls).view(-1, 1).cuda(), 0
        )
        mask = mask * logits_mask

        exp_logits = torch.exp(self.args.r*logits+self.args.margin) 
        mean_prob_neg = torch.log((mask*exp_logits).sum(1)/mask.sum(1))
        mean_prob_neg = mean_prob_neg[~torch.isnan(mean_prob_neg)]
        loss = self.temperature / self.base_temperature * mean_prob_neg.mean()
        return loss

    def init_class_prototypes(self):
        """Initialize class prototypes"""
        self.model.eval()
        start = time.time()
        prototype_counts = [0]*self.args.n_cls
        with torch.no_grad():
            prototypes = torch.zeros(self.args.n_cls, self.args.feat_dim).cuda()
            feat_list = [[] for _ in range(self.args.n_cls)]
            for i, (input, target) in enumerate(self.loader):
                input, target = input.cuda(), target.cuda()
                features = self.model.head_forward(input)
                if self.args.head == "hypb":
                    for j, feature in enumerate(features):
                        feat_list[target[j].item()].append(features) 
                else: 
                    for j, feature in enumerate(features):
                        prototypes[target[j].item()] += feature
                        prototype_counts[target[j].item()] += 1

            if self.args.head == "hypb":
                for cls, features in enumerate(feat_list):
                    prototypes[cls] = hypb.poincare_mean(torch.vstack(features), 
                        dim=0, 
                        c=self.args.c_ball
                    )
            else:
                for cls in range(self.args.n_cls):
                    prototypes[cls] /=  prototype_counts[cls] 

            # measure elapsed time
            duration = time.time() - start
            print(f'Time to initialize prototypes: {duration:.3f}')
            prototypes = F.normalize(prototypes, dim=1)
            self.prototypes = prototypes


class RegLoss(nn.Module):
    '''
    Disparity Loss on other class samples to the prototypes
    '''
    def __init__(self, args):
        super(RegLoss, self).__init__()
        self.args = args
        self.temperature = args.temp

    def forward(self, features, labels, prototypes):    
        
        proxy_labels = torch.arange(0, self.args.n_cls).cuda()
        labels = labels.contiguous().view(-1, 1)
        mask = 1-torch.eq(labels, proxy_labels.T).float().cuda() #bz, cls

        # compute logits
        if self.args.head == "hypb":
            feat_dot_prototype = -hypb.dist_matrix(features, prototypes, c=self.args.c_ball)
        else:
            prototypes = F.normalize(prototypes, dim=1) 
            feat_dot_prototype = torch.matmul(features, prototypes.T)
        feat_dot_prototype = torch.div(feat_dot_prototype, self.temperature)
        
        # for numerical stability
        logits_max, _ = torch.max(feat_dot_prototype, dim=1, keepdim=True)
        logits = feat_dot_prototype - logits_max.detach()

        # compute log_prob
        exp_logits = torch.exp(self.args.r*logits+self.args.margin) 
        log_prob = torch.log(exp_logits.sum(1, keepdim=True))

        # compute mean of log-likelihood over positive
        mean_log_prob_pos = (mask * log_prob).sum(1) 

        # loss
        loss = self.temperature * mean_log_prob_pos.mean()
        return loss


# class RegLoss(nn.Module):
#     '''
#     Regularization Loss on prototypes map to idea positions
#     '''
#     def __init__(self, args):
#         super(RegLoss, self).__init__()
#         self.args = args
#         self.temperature = args.temp
# 
#         proto_dir = f"{args.main_dir}/prototype/"
#         proto_path = os.path.join(proto_dir, "prototypes-%dd-%dc.npy"%(args.feat_dim, args.n_cls))
#         if os.path.exists(proto_path):
#             self.ideal_protos = torch.from_numpy(np.load(proto_path)).float()
#         else:
#             self.ideal_protos = initialize_prototype(args)
#             os.makedirs(proto_dir, exist_ok=True)
#             np.save(proto_path, self.ideal_protos.data.numpy())
#         self.ideal_protos = self.ideal_protos.cuda()
# 
#     def forward(self, prototypes):    
#         labels = torch.arange(0, self.args.n_cls).cuda()
#         logits = torch.matmul(prototypes, self.ideal_protos[labels].T)
#         logits = torch.div(logits, self.temperature)
# 
#         labels = labels.contiguous().view(-1, 1)
#         mask = (1-torch.eq(labels, labels.T).float()).cuda()
# 
#         logits_mask = torch.scatter(
#             torch.ones_like(mask),
#             1,
#             torch.arange(self.args.n_cls).view(-1, 1).cuda(),
#             0
#         )
#         mask = mask * logits_mask
#         mean_prob_neg = -torch.log((mask * torch.exp(logits)).sum(1) / mask.sum(1))
#         mean_prob_neg = mean_prob_neg[~torch.isnan(mean_prob_neg)]
#         # bug: be careful of the temp
#         loss = mean_prob_neg.mean()
#         return loss


def trasnform_func(x, t=2.0):
    out = 2. * ((x + 1.) / 2.).pow(t) - 1.
    return out


class SphereFace2(nn.Module):
    """ reference: <SphereFace2: Binary Classification is All You Need
                    for Deep Face Recognition>
        margin='C' -> SphereFace2-C
        margin='A' -> SphereFace2-A
        marign='M' -> SphereFAce2-M
    """
    def __init__(self, args, magn_type='C',
            alpha=0.7, r=40., m=0.4, t=5., lw=10.):
        super().__init__()
        self.feat_dim = args.feat_dim
        self.num_class = args.n_cls
        self.magn_type = magn_type

        # alpha is the lambda in paper Eqn. 5
        self.alpha = alpha
        self.r = r
        self.m = m
        self.t = t
        self.lw = lw

        # init weights
        self.w = nn.Parameter(torch.Tensor(self.feat_dim, self.num_class))
        nn.init.xavier_normal_(self.w)

        # init bias
        z = alpha / ((1. - alpha) * (self.num_class - 1.))
        if magn_type == 'C':
            ay = r * (2. * 0.5**t - 1. - m)
            ai = r * (2. * 0.5**t - 1. + m)
        elif magn_type == 'A':
            theta_y = min(math.pi, math.pi/2. + m)
            ay = r * (2. * ((math.cos(theta_y) + 1.) / 2.)**t - 1.)
            ai = r * (2. * 0.5**t - 1.)
        elif magn_type == 'M':
            theta_y = min(math.pi, m * math.pi/2.)
            ay = r * (2. * ((math.cos(theta_y) + 1.) / 2.)**t - 1.)
            ai = r * (2. * 0.5**t - 1.)
        else:
            raise NotImplementedError

        temp = (1. - z)**2 + 4. * z * math.exp(ay - ai)
        b = (math.log(2. * z) - ai
             - math.log(1. - z +  math.sqrt(temp)))
        self.b = nn.Parameter(torch.Tensor(1))
        nn.init.constant_(self.b, b)

    def forward(self, x, y):
        with torch.no_grad():
            self.w.data = F.normalize(self.w.data, dim=0)


        # if len(x.shape) < 3:
        #     raise ValueError('`features` needs to be [bsz, n_views, ...],'
        #                      'at least 3 dimensions are required')
        if len(x.shape) > 3:
            x = x.view(x.shape[0], x.shape[1], -1)

        #delta theta with margin
        cos_theta = F.normalize(x, dim=1).mm(self.w)
        one_hot = torch.zeros_like(cos_theta)
        one_hot.scatter_(1, y.view(-1, 1), 1.)
        with torch.no_grad():
            if self.magn_type == 'C':
                # g_cos_theta = 2. * ((cos_theta + 1.) / 2.).pow(self.t) - 1.
                g_cos_theta = self.trasnform_func(cos_theta)
                g_cos_theta = g_cos_theta - self.m * (2. * one_hot - 1.)
            elif self.magn_type == 'A':
                theta_m = torch.acos(cos_theta.clamp(-1+1e-5, 1.-1e-5))
                theta_m.scatter_(1, y.view(-1, 1), self.m, reduce='add')
                theta_m.clamp_(1e-5, 3.14159)
                g_cos_theta = torch.cos(theta_m)
                # g_cos_theta = 2. * ((g_cos_theta + 1.) / 2.).pow(self.t) - 1.
                g_cos_theta = self.trasnform_func(g_cos_theta)
            elif self.magn_type == 'M':
                m_theta = torch.acos(cos_theta.clamp(-1+1e-5, 1.-1e-5))
                m_theta.scatter_(1, y.view(-1, 1), self.m, reduce='multiply')
                m_theta.clamp_(1e-5, 3.14159)
                g_cos_theta = torch.cos(m_theta)
                #g_cos_theta = 2. * ((g_cos_theta + 1.) / 2.).pow(self.t) - 1.
                g_cos_theta = self.trasnform_func(g_cos_theta)
            else:
                raise NotImplementedError
            d_theta = g_cos_theta - cos_theta
        
        logits = self.r * (cos_theta + d_theta) + self.b
        weight = self.alpha * one_hot + (1. - self.alpha) * (1. - one_hot)
        weight = self.lw * self.num_class / self.r * weight
        loss = F.binary_cross_entropy_with_logits(
                logits, one_hot, weight=weight)

        return loss

    def trasnform_func(self, x):
        out = 2. * ((x + 1.) / 2.).pow(self.t) - 1.
        return out
        


class SphereFacePlus(nn.Module):
    """ reference: <Learning towards Minimum Hyperspherical Energy>"
    """
    def __init__(self, args, s=30., m=1.5, lambda_MHE=1.):
        super(SphereFacePlus, self).__init__()
        self.feat_dim = args.feat_dim
        self.num_class = args.n_cls
        self.s = s
        self.m = m
        self.lambda_MHE = lambda_MHE
        self.w = nn.Parameter(torch.Tensor(feat_dim, num_class))
        nn.init.xavier_normal_(self.w)

    def forward(self, x, y):
        # weight normalization
        with torch.no_grad():
            self.w.data = F.normalize(self.w.data, dim=0)

        # cos_theta and d_theta
        cos_theta = F.normalize(x, dim=1).mm(self.w)
        with torch.no_grad():
            m_theta = torch.acos(cos_theta.clamp(-1.+1e-5, 1.-1e-5))
            m_theta.scatter_(
                1, y.view(-1, 1), self.m, reduce='multiply',
            )
            k = (m_theta / math.pi).floor()
            sign = -2 * torch.remainder(k, 2) + 1  # (-1)**k
            phi_theta = sign * torch.cos(m_theta) - 2. * k
            d_theta = phi_theta - cos_theta

        logits = self.s * (cos_theta + d_theta)

        # mini-batch MHE loss for classifiers
        sel_w = self.w[:,torch.unique(y)]
        gram_mat = torch.acos(torch.matmul(torch.transpose(sel_w, 0, 1), sel_w).clamp(-1.+1e-5, 1.-1e-5))
        shape_gram = gram_mat.size()
        MHE_loss = torch.sum(torch.triu(torch.pow(gram_mat, -2), diagonal=1))
        MHE_loss = MHE_loss / (shape_gram[0] * (shape_gram[0] - 1) * 0.5)

        loss = F.cross_entropy(logits, y) + self.lambda_MHE * MHE_loss

        return loss
